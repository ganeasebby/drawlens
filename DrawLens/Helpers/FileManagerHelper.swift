//
//  FileManagerHelper.swift
//  DrawLens
//
//  Created by Sebastian Ganea on 18/01/2021.
//

import UIKit

class FileManagerHelper: FileManager {

    static let documentsFolderUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    static let arImagesFolder = documentsFolderUrl.appendingPathComponent("ARImages")
    static let arResourcesFolder = documentsFolderUrl.appendingPathComponent("ARData")
    static let projectedImage = arImagesFolder.appendingPathComponent("imageToProject").appendingPathExtension("png")
    static let imageToTrack = arImagesFolder.appendingPathComponent("imageToTrack").appendingPathExtension("png")
    static let mappedWorldFile = arResourcesFolder.appendingPathComponent("worldMap").appendingPathExtension("dlar")
    static let mainNodeAnchorFile = arResourcesFolder.appendingPathComponent("mainAnchor").appendingPathExtension("dlar")
    static let mainNodeFile = arResourcesFolder.appendingPathComponent("mainNode").appendingPathExtension("dlar")
    
    class func copyTheImagePlaceHolderIfNeeded(){
        if self.default.fileExists(atPath: arImagesFolder.path) == false{
            /// the folder does not exist.... create the folder and copy the file
            
            do{
                try self.default.createDirectory(at: arImagesFolder, withIntermediateDirectories: true, attributes: nil)
            }
            catch{
                NSLog("Could not create 'arImagesFolder' in documents directory... error\(error)")
            }
            
        }
        
        if self.default.fileExists(atPath: arResourcesFolder.path) == false{
            /// the folder does not exist.... create the folder and copy the file
            
            do{
                try self.default.createDirectory(at: arResourcesFolder, withIntermediateDirectories: true, attributes: nil)
            }
            catch{
                NSLog("Could not create 'arImagesFolder' in documents directory... error\(error)")
            }
            
        }
        
        
        /// check if the placeholder exists, if not copy it
        if self.default.fileExists(atPath: projectedImage.path) == false{
            
            do{
                if let placeHolderResource = Bundle.main.url(forResource: "grid", withExtension: "png"){
                    try self.default.copyItem(at: placeHolderResource, to: projectedImage)
                }
            }
            catch{
                NSLog("Could not copy the placeholder image to 'arImagesFolder' in documents directory... error\(error)")
            }
        }
        
        
    }
    class func getARImageUsedForProjection() -> UIImage{
        
        let image = UIImage(contentsOfFile: projectedImage.path)
        
        return image!
    }
    class func getImageUsedForTracking() -> UIImage?{
        let image = UIImage(contentsOfFile: imageToTrack.path)
        
        return image
    }
    class func updateTheImageToProject(_ image: UIImage){
        /// delete old image
        do{
            try self.default.removeItem(at: projectedImage)
        }
        catch{
            NSLog("could not delete old image reason: \(error)")
        }
        
        /// write the image at path
        do{
            try image.pngData()?.write(to: projectedImage)
        }
        catch{
            NSLog("could not save the new image... the placeholder image will be used instead. Error:\(error)")
            copyTheImagePlaceHolderIfNeeded()
        }
        
    
    }
    class func updateTheImageUsedForTracking(_ image: UIImage){
        /// delete old image
        do{
            try self.default.removeItem(at: imageToTrack)
        }
        catch{
            NSLog("could not delete old image reason: \(error)")
        }
        
        /// write the image at path
        do{
            try image.pngData()?.write(to: imageToTrack)
        }
        catch{
            NSLog("could not save the new image... Error:\(error)")
        }
    }
    
    class func saveMappedWorld(_ mappedWorld : Data){
//        /// delete old file
//        do{
//            try self.default.removeItem(at: mappedWorldFile)
//        }
//        catch{
//            NSLog("could not delete old image reason: \(error)")
//        }
        
        /// write the file at path
        do{
            try mappedWorld.write(to: mappedWorldFile)
        }
        catch{
            NSLog("could not save the file. Error:\(error)")
            
        }
    }
    class func saveMainNodeAnchor(_ nodeAnchor : Data){
        /// write the file at path
        do{
            try nodeAnchor.write(to: mainNodeAnchorFile)
        }
        catch{
            NSLog("could not save the file. Error:\(error)")
            
        }
    }
    class func saveMainNode(_ node : Data){        
        /// write the file at path
        do{
            try node.write(to: mainNodeFile)
        }
        catch{
            NSLog("could not save the file. Error:\(error)")
            
        }
    }
    class func retriveMappedWorldData() -> Data?{
        if self.default.fileExists(atPath: mappedWorldFile.path){
            do{
                let data = try Data(contentsOf: mappedWorldFile)
                return data
            }
            catch{
                NSLog("FileManager --> there was an error trying to retrieve mapped world data... Error: \(error)")
            }
            
        }
        else{
            NSLog("FileManager --> mapped world data file does not exist")
        }
        
        return nil
    }
    class func retriveMainNodeData() -> Data?{
        if self.default.fileExists(atPath: mainNodeFile.path){
            do{
                let data = try Data(contentsOf: mainNodeFile)
                return data
            }
            catch{
                NSLog("FileManager --> there was an error trying to retrieve the main node data... Error: \(error)")
            }
            
        }
        else{
            NSLog("FileManager --> main node data file does not exist")
        }
        
        return nil
    }
}
