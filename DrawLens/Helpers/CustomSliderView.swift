//
//  CustomSliderView.swift
//  DrawLens
//
//  Created by Sebastian Ganea on 13/01/2021.
//

import UIKit
enum SliderTypeEnum {
    case nodeScale
    case transparency
}

class CustomSliderView: UIView {
    
    let slider = UISlider()
    let label = UILabel()
    var sliderValueCallBackFunction : ((CGFloat) -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.frame = self.bounds
        label.frame.size.height = self.bounds.height * 0.3
        
        slider.frame = CGRect(x: 0, y: label.frame.origin.y + label.frame.height, width: self.bounds.width, height: self.bounds.height - (label.frame.origin.y + label.frame.height))
        slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        
        self.addSubview(slider)
        self.addSubview(label)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(type: SliderTypeEnum, frame: CGRect) {
        self.init(frame: frame)
        
        if type == .nodeScale {
            setupNodeScale()
        }
        else if type == .transparency{
            
        }
    }
    
    
    func setupNodeScale(){
        slider.transform = CGAffineTransform(rotationAngle: .pi / -2)
    }
    
    
    @objc func sliderValueChanged(){
        label.text = String(slider.value)
        sliderValueCallBackFunction?(CGFloat(slider.value))
    }

}
