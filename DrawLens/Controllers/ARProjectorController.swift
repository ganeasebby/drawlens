//
//  ARProjectorController.swift
//  DrawLens
//
//  Created by Sebastian Ganea on 13/01/2021.
//

import UIKit
import SceneKit
import ARKit
import MultiPeer
import ProgressHUD

class ARProjectorController: UIViewController, ARSCNViewDelegate, ARSessionDelegate, UITextFieldDelegate,  MultiPeerDelegate {
    
    enum MultiPeerDataType: UInt32{
        case string = 0
        case image = 1
        case data = 2
        case mappedWorld = 3
        case anchor = 4
        case node = 5
    }
    
    enum ARConfigurationMode: String{
        case worldTracking = "world tracking"
        case imageTracking = "image tracking"
    }

    /// the current scene
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var sendARWorldButton: UIButton!
    @IBOutlet weak var worldMappingStatusLabel: UILabel!
    @IBOutlet weak var connectedDevicesCountLabel: UILabel!
    @IBOutlet weak var connectedDeviceName: UILabel!
    
    /// the currently displayed node.... if one is displayed at the moment
    var displayedNode: SCNNode?
    
    /// current rotationAngle of the main node
    var nodeRotationAngle: Float = 0.0
    
    /// what does the app use to anchor the projected image into the real world
    var arAnchorMode = ARConfigurationMode.worldTracking{
        didSet{
            crtTrackingModeLabel.text = "ARKit is curently using '\(arAnchorMode.rawValue)' to anchor the projected image"
            crtTrackingModeLabel.frame.origin = CGPoint(x: 5, y: 5)
            
            ProgressHUD.show("AR config changed to:  \(arAnchorMode.rawValue)", icon: .succeed, interaction: true)
        }
    }
    
    /// a label used to display what is the current AR tracking/anchoring mode
    let crtTrackingModeLabel = UILabel()
    
    /// a placeholder depicting the image we are trying to track
    let imagetracking = UIImageView()
    
    //MARK: - init
    override func viewDidLoad() {
        super.viewDidLoad()

        /// initialize the sceene
        sceneView.delegate = self
        sceneView.showsStatistics = true
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        sceneView.scene = scene
        
        
        /// make sure that the node image placeholder exists in documents directory
        FileManagerHelper.copyTheImagePlaceHolderIfNeeded()
        
        /// initialize info label
        sceneView.addSubview(crtTrackingModeLabel)
        crtTrackingModeLabel.frame.size = CGSize(width: sceneView.frame.width - 10, height: 20)
        crtTrackingModeLabel.adjustsFontSizeToFitWidth = true
        
        arAnchorMode = .worldTracking
        
        /// info image tracking
        if let image = FileManagerHelper.getImageUsedForTracking(){
            imagetracking.image = image
        }
        imagetracking.frame = CGRect(x: 5, y: sceneView.frame.height - 125, width: 100, height: 100)
        imagetracking.contentMode = .scaleAspectFit
        sceneView.addSubview(imagetracking)
        
        /// add transparency slider
        let origin = imagetracking.frame.origin.x + imagetracking.frame.width + 20
        let slider = CustomSliderView(type: .transparency, frame: CGRect(x: origin, y: imagetracking.center.y, width: (sceneView.frame.width - 10) - origin, height: 50))
        slider.sliderValueCallBackFunction = changeTheNodeAlphaValue(_:)
        sceneView.addSubview(slider)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = configureARWith(trackingMode: arAnchorMode)
        initiateSceneSessionWithConfiguration(configuration)
        
        addGestures()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /// initialize the Multipeer connectivity
        MultiPeer.instance.initialize(serviceType: "DLConn")
        MultiPeer.instance.autoConnect()
        MultiPeer.instance.delegate = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    func addGestures () {
        let tapped = UITapGestureRecognizer(target: self, action: #selector(tapGesture))
        sceneView.addGestureRecognizer(tapped)
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture))
        sceneView.addGestureRecognizer(pinch)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panGesture))
        sceneView.addGestureRecognizer(pan)
        
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(rotateGesture))
        sceneView.addGestureRecognizer(rotate)
    }
    func configureARWith( trackingMode: ARConfigurationMode) -> ARConfiguration{
        if trackingMode == .worldTracking {
            
            let configuration = ARWorldTrackingConfiguration()
            configuration.planeDetection = .vertical
            return configuration
        }
        else{
//            let configuration = ARWorldTrackingConfiguration()
            let configuration = ARImageTrackingConfiguration()
            if let image = FileManagerHelper.getImageUsedForTracking(){
                let size = image.pngData()!.count
                let key = String(size)
                
                
                if let  strValue = UserDefaults.standard.string(forKey: key){
                    let floatValue = CGFloat(Int(strValue)!)
                    let arRefimage = ARReferenceImage(image.cgImage!, orientation: .up, physicalWidth: floatValue / 100)
//                    configuration.detectionImages = [arRefimage]
                    configuration.trackingImages = [arRefimage]
                    
                    imagetracking.image = image
                }
                else{
                    ProgressHUD.show("Could not generate the correct configuration, because ARimageReference width was not found", icon: AlertIcon.exclamation, interaction: false)
                }
            }
            else{
                ProgressHUD.show("No tracking image found. Please set a image to track.", icon: AlertIcon.exclamation, interaction: false)
            }
            return configuration
        }
    }
    func initiateSceneSessionWithConfiguration(_ config: ARConfiguration){
        self.sceneView.autoenablesDefaultLighting = true
        
        // Prevent the screen from being dimmed after a while as users will likely
        // have long periods of interaction without touching the screen or buttons.
        UIApplication.shared.isIdleTimerDisabled = true
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
        sceneView.session.delegate = self
        
        
        // Run the view's session
        sceneView.session.run(config)
    
    }
    func runSesstionWithWorldMap(_ mappedWorld: ARWorldMap){
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .vertical
        configuration.initialWorldMap = mappedWorld
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors, .resetSceneReconstruction])
    }
    
    //MARK: - Dev
    @IBAction func devActionPressed(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Try out some dev Actions!", message: "What could go wrong?", preferredStyle: .actionSheet)
        
        let setNodeImage = UIAlertAction(title: "Change the image you wish to project", style: .default) { (action) in
            self.changeProjectionImage()
        }
        let setNodeImage2 = UIAlertAction(title: "Change the image you wish to project to 'Grid image'", style: .default) { (action) in
            self.changeProjectionImageToGrid()
        }
        let setImageToTrack = UIAlertAction(title: "Change the image you wish to track", style: .default) { (action) in
            self.changeTrackingImage()
        }
        let pingOtherDevice = UIAlertAction(title: "Ping other connected devices", style: .default) { (action) in
            self.pingOtherConnectedDevices()
        }
        let imageTracking = UIAlertAction(title: "Change AR configuration to imageTracking", style: .default) { (action) in
            self.removeAllNodes()
            self.arAnchorMode = .imageTracking
            let config = self.configureARWith(trackingMode: self.arAnchorMode)
            self.initiateSceneSessionWithConfiguration(config)
        }
        let worldTracking = UIAlertAction(title: "Change AR configuration to world tracking", style: .default) { (action) in
            self.removeAllNodes()
            self.arAnchorMode = .worldTracking
            let config = self.configureARWith(trackingMode: self.arAnchorMode)
            self.initiateSceneSessionWithConfiguration(config)
        }
        let sendCurrentmappedState = UIAlertAction(title: "Send current state of the AR world to connected devices", style: .default) { (action) in
            guard self.sendARWorldButton.isEnabled else{
                ProgressHUD.show("Map more of the real world. Move your device arround. And make sure there are other devices connected.", icon: .failed, interaction: true)
                return
            }
            
            
            self.sceneView.session.getCurrentWorldMap { worldMap, error in
                guard let map = worldMap
                    else { print("Error: \(error!.localizedDescription)"); return }
                
                if let data = self.encodeMappedWorld(map){
                    MultiPeer.instance.send(data: data, type: MultiPeerDataType.mappedWorld.rawValue)
                }
                
                if self.displayedNode != nil{
                    if let data = self.encodeNode(self.displayedNode!){
                        MultiPeer.instance.send(data: data, type: MultiPeerDataType.node.rawValue)
                    }
                }

            }

        }
        let saveState = UIAlertAction(title: "Save curent AR world to file", style: .default) { (action) in
            
            let worldMappingStatus = self.sceneView.session.currentFrame?.worldMappingStatus
            if worldMappingStatus == nil || worldMappingStatus! == .notAvailable || worldMappingStatus! == .limited{
                ProgressHUD.show("Map more od the real world. Move your device arround.", icon: .failed, interaction: true)
                return
            }


            /// world map
            self.sceneView.session.getCurrentWorldMap { worldMap, error in
                guard let map = worldMap else {
                    print("Error: \(error!.localizedDescription)");
                    return
                }
                
                if let data = self.encodeMappedWorld(map){
                    FileManagerHelper.saveMappedWorld(data)
                }
            }

            if self.displayedNode != nil{
                if let data = self.encodeNode(self.displayedNode!){
                    FileManagerHelper.saveMainNode(data)
                }
            }
            
            
        }
        let restoreState = UIAlertAction(title: "Restore AR world from file", style: .default) { (action) in
            
            guard let worldStateData = FileManagerHelper.retriveMappedWorldData() else{
                ProgressHUD.show("world data could not be retrived. Aborting....", icon: .failed, interaction: true)
                return
            }
            
            guard let worldState = self.decodeMappedWorld(worldStateData) else{
                ProgressHUD.show("world data could not be decoded. Aborting....", icon: .failed, interaction: true)
                return
            }
            
            /// run the session with this mappt world
            self.runSesstionWithWorldMap(worldState)
            
            if let nodeData = FileManagerHelper.retriveMainNodeData(){
                if let node = self.decodeNode(nodeData){
                    self.sceneView.scene.rootNode.addChildNode(node)
                    self.displayedNode = node
                }
            }
            
            
        }
        let removeBlueMesh = UIAlertAction(title: "Remove blue meshes", style: .default) { (action) in
            
            /// remove blue mashes
            self.sceneView.scene.rootNode.enumerateChildNodes { (child, _) in
                if child.name == "MeshNode" || child.name == "TextNode"{
                    child.removeFromParentNode()
                }
            }
            
            
        }
        let dismiss = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        

        actionSheet.addAction(setNodeImage)
        actionSheet.addAction(setNodeImage2)
        actionSheet.addAction(setImageToTrack)
        actionSheet.addAction(pingOtherDevice)
        actionSheet.addAction(worldTracking)
        actionSheet.addAction(imageTracking)
        actionSheet.addAction(sendCurrentmappedState)
        actionSheet.addAction(saveState)
        actionSheet.addAction(restoreState)
        actionSheet.addAction(removeBlueMesh)
        
        
        actionSheet.addAction(dismiss)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: - Runtime
    func setTheWidthForTheTrackingImage(_ image: UIImage){
        let textField = UITextField(frame: CGRect(x: 5, y: 40, width: sceneView.frame.width - 10, height: 30))
        textField.font = UIFont.systemFont(ofSize: 11)
        textField.placeholder = "Enter in centimeters the width of the picture in the real world"
        textField.returnKeyType = .done
        textField.keyboardType = .numbersAndPunctuation
        textField.backgroundColor = .white
        sceneView.addSubview(textField)
        textField.delegate = self
        
        textField.becomeFirstResponder()
        
    }
    func changeTheNodeAlphaValue(_ alpha: CGFloat){
        displayedNode?.geometry?.firstMaterial?.transparency = alpha
    }
    
    //MARK: - Gestures
    @objc func tapGesture (gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: sceneView)
        guard let query = sceneView.raycastQuery(from: location, allowing: .existingPlaneInfinite, alignment: .any) else {
           return
        }

        let results = sceneView.session.raycast(query)
        guard let hitTestResult = results.first else {
           print("No surface found")
           return
        }
        let columns = hitTestResult.worldTransform.columns.3
        let position = SCNVector3(x: columns.x, y: columns.y, z: columns.z)
       

        removeAllNodes()

        placeImageAtPosition(position, worldTransform: hitTestResult.worldTransform)

    }
    @objc func pinchGesture (gesture: UIPinchGestureRecognizer){
        let location = gesture.location(in: sceneView)
        let hitTestResults = sceneView.hitTest(location)
        guard let nodeToScale = hitTestResults.first?.node, nodeToScale == displayedNode else {
            return
        }
        
        if gesture.state == .changed {
            let pinchScaleX: CGFloat = gesture.scale * CGFloat((nodeToScale.scale.x))
            let pinchScaleY: CGFloat = gesture.scale * CGFloat((nodeToScale.scale.y))
            let pinchScaleZ: CGFloat = gesture.scale * CGFloat((nodeToScale.scale.z))
            nodeToScale.scale = SCNVector3Make(Float(pinchScaleX), Float(pinchScaleY), Float(pinchScaleZ))
            gesture.scale = 1
            
//            if let planeGeometry = nodeToScale.geometry as? SCNPlane{
//                print("current size of the image is \(planeGeometry.width * CGFloat(nodeToScale.scale.x)) X \(planeGeometry.height * CGFloat(nodeToScale.scale.y)) meters")
//            }
            
            //                print("\(nodeToScale.frame.size)")
        }
        
    }
    @objc func panGesture(gesture: UIPanGestureRecognizer){
        let location = gesture.location(in: sceneView)
        
        let hitTestResults = sceneView.hitTest(location)
        guard let node = hitTestResults.first?.node, node == displayedNode else {
            /// don't go any further since the corect node was not touched
            return
        }
        
        guard let query = sceneView.raycastQuery(from: location, allowing: .existingPlaneInfinite, alignment: .any) else {
           return
        }

        let results = sceneView.session.raycast(query)
        guard let hitTestResult = results.first else {
           print("No surface found")
           return
        }
        let columns = hitTestResult.worldTransform.columns.3
        let position = SCNVector3(x: columns.x, y: columns.y, z: columns.z)
        
        node.position = position
    }
    @objc func rotateGesture(gesture: UIRotationGestureRecognizer){
        let location = gesture.location(in: sceneView)
        
        let hitTestResults = sceneView.hitTest(location)
        guard let node = hitTestResults.first?.node, node == displayedNode else {
            /// don't go any further since the corect node was not touched
            return
        }
        
        let rotation = Float(gesture.rotation)

        if gesture.state == .changed{
            node.rotation.z = nodeRotationAngle + rotation
        }

        if(gesture.state == .ended) {
            nodeRotationAngle = node.rotation.z

         }
    }
    
    //MARK: - Actions
    func removeAllNodes(){
        sceneView.scene.rootNode.enumerateChildNodes { (child, _) in
            if child.name == "MeshNode" || child.name == "TextNode" || child == displayedNode{
                child.removeFromParentNode()
            }
        }
        
//        while sceneView.scene.rootNode.childNodes.count > 0 {
//            if let last = sceneView.scene.rootNode.childNodes.last{
//                last.removeFromParentNode()
//            }
//        }
    }
    func placeImageAtPosition(_ position: SCNVector3, worldTransform: simd_float4x4){
        ///create the shape of the node
        let shape = SCNPlane(width: 1, height: 1)
        
        ///create the material of the node
        let material = SCNMaterial()
//        material.diffuse.contents = UIImage(named: "TMT.png")
        /// set the pattern of the material
//        material.diffuse.contents = UIImage(named: "liam.png")
        material.diffuse.contents = FileManagerHelper.getARImageUsedForProjection()
        shape.materials = [material]
        
        /// create the node based on the shape
        displayedNode = SCNNode(geometry: shape)
        displayedNode!.position = position
        displayedNode!.name = "our custom image node"
        
        /// rotate the node according to the plane
        displayedNode?.eulerAngles = SCNVector3(x: Float(0.degreesToradians), y: 0, z: 0)
        
//        /// rotate the node to face the camera
//        guard let frame = self.sceneView.session.currentFrame else {
//            return
//        }
//        displayedNode!.eulerAngles.y = frame.camera.eulerAngles.y
        
        /// place the node on the scene
        sceneView.scene.rootNode.addChildNode(displayedNode!)
        
        
        // Place an anchor for a virtual character. The model appears in renderer(_:didAdd:for:).
        let anchor = ARAnchor(name: "placedImage", transform: worldTransform)
        
        // Send the anchor info to peers, so they can place the same content.
        guard let anchorData = try? NSKeyedArchiver.archivedData(withRootObject: anchor, requiringSecureCoding: true) else {
            fatalError("can't encode anchor")
        }
        
        guard let nodeData = try? NSKeyedArchiver.archivedData(withRootObject: displayedNode!, requiringSecureCoding: true) else {
            fatalError("can't encode node")
        }
        
        MultiPeer.instance.send(data: anchorData, type: MultiPeerDataType.anchor.rawValue)
        MultiPeer.instance.send(data: nodeData, type: MultiPeerDataType.node.rawValue)
          
    }
    func placeBallAtPosition(_ position: SCNVector3){
        let ballShape = SCNSphere(radius: 0.1)
        
        let ballNode = SCNNode(geometry: ballShape)
        ballNode.position = position
        
        sceneView.scene.rootNode.addChildNode(ballNode)
    }
    func placeAirplaneAtPosition(_ position: SCNVector3){
        
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        let node = scene.rootNode.childNode(withName: "ship", recursively: false)
        node!.position = position
        sceneView.scene.rootNode.addChildNode(node!)
    }
    func showSlider(){
        let width: CGFloat = 80
        let height: CGFloat = 150
        let sliderView = CustomSliderView(type: .nodeScale, frame: CGRect(x: self.view.frame.width - width, y: self.view.bounds.height / 2, width: width, height: height))
        self.view.addSubview(sliderView)
    }
    func changeProjectionImage(){
        let picker = ImagePicker()
        picker.imageSelecterCallback = { (_ image: UIImage) in
            let newMaterial = SCNMaterial()
            newMaterial.diffuse.contents = image
            self.displayedNode?.geometry?.materials = [newMaterial]
            
            /// save new picked image to documents
            FileManagerHelper.updateTheImageToProject(image)
            
            ProgressHUD.show("Projected image was updated", icon: .succeed, interaction: true)
        }
        self.present(picker, animated: true, completion: nil)
    }
    func changeProjectionImageToGrid(){
        let image = UIImage(named: "grid.png")
        FileManagerHelper.updateTheImageToProject(image!)
        
        let newMaterial = SCNMaterial()
        newMaterial.diffuse.contents = image
        self.displayedNode?.geometry?.materials = [newMaterial]
        

    }
    func changeTrackingImage(){
        let picker = ImagePicker()
        picker.imageSelecterCallback = { (_ image: UIImage) in
            FileManagerHelper.updateTheImageUsedForTracking(image)
            self.setTheWidthForTheTrackingImage(image)
        }
        self.present(picker, animated: true, completion: nil)
    }
    func pingOtherConnectedDevices(){
        let message = "\(UIDevice.current.name) sais helllllou"
        if let data = message.data(using: .utf8){
            MultiPeer.instance.send(data: data, type: MultiPeerDataType.string.rawValue)
        }
        
    }
    @IBAction func sendMapprdARWorld(_ sender: UIButton) {
        sceneView.session.getCurrentWorldMap { worldMap, error in
            guard let map = worldMap
                else { print("Error: \(error!.localizedDescription)"); return }
            
            if let data = self.encodeMappedWorld(map){
                MultiPeer.instance.send(data: data, type: MultiPeerDataType.mappedWorld.rawValue)
            }
            
            if self.displayedNode != nil{
                if let nodeData = self.encodeNode(self.displayedNode!){
                    MultiPeer.instance.send(data: nodeData, type: MultiPeerDataType.node.rawValue)
                }
            }
            

        }
    }
    
    //MARK: - SceneView delegate
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        let meshNode : SCNNode
        let textNode : SCNNode
        guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
        
        
        guard let meshGeometry = ARSCNPlaneGeometry(device: sceneView.device!)
        else {
            fatalError("Can't create plane geometry")
        }
        meshGeometry.update(from: planeAnchor.geometry)
        meshNode = SCNNode(geometry: meshGeometry)
        meshNode.opacity = 0.6
        meshNode.name = "MeshNode"
        
        guard let material = meshNode.geometry?.firstMaterial
        else { fatalError("ARSCNPlaneGeometry always has one material") }
        material.diffuse.contents = UIColor.blue
        
        node.addChildNode(meshNode)
        
        let textGeometry = SCNText(string: "Plane", extrusionDepth: 1)
        textGeometry.font = UIFont(name: "Futura", size: 75)
        
        textNode = SCNNode(geometry: textGeometry)
        textNode.name = "TextNode"
        
        textNode.simdScale = SIMD3(repeating: 0.0005)
        textNode.eulerAngles = SCNVector3(x: Float(-90.degreesToradians), y: 0, z: 0)
        
        node.addChildNode(textNode)
        
        textNode.centerAlign()
        
        
        print("did add plane node")
        
    }
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        let planeNode = node.childNode(withName: "MeshNode", recursively: false)
        
        if let planeGeometry = planeNode?.geometry as? ARSCNPlaneGeometry {
            planeGeometry.update(from: planeAnchor.geometry)
        }
        
    }

    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        
        if let imageAnchor = anchor as? ARImageAnchor{
            let size = imageAnchor.referenceImage.physicalSize
            let plane = SCNPlane(width: size.width, height: size.height)
            plane.firstMaterial?.diffuse.contents = FileManagerHelper.getARImageUsedForProjection()
            
//            let planeNode = SCNNode(geometry: plane)
//            planeNode.eulerAngles.x = -.pi / 2
//            node.addChildNode(planeNode)
            
            displayedNode = SCNNode(geometry: plane)
            displayedNode?.eulerAngles.x = -.pi / 2
            node.addChildNode(displayedNode!)
            
            
        }
        
        return node
    }
    
    //MARK: - ARSession delegate
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        var status = "Unknown"
        switch frame.worldMappingStatus {
        case .notAvailable, .limited:
            sendARWorldButton.isEnabled = false
            status = "Limited"
        case .extending:
            sendARWorldButton.isEnabled = MultiPeer.instance.isConnected
            status = "Extending"
        case .mapped:
            sendARWorldButton.isEnabled = MultiPeer.instance.isConnected
            status = "Mapped"
        @unknown default:
            sendARWorldButton.isEnabled = false
            status = "Unknown"
        }
        worldMappingStatusLabel.text = status
//        updateSessionInfoLabel(for: frame, trackingState: frame.camera.trackingState)
    }
    
    //MARK: - Multipeer delegate
    func multiPeer(didReceiveData data: Data, ofType type: UInt32, from peerID: MCPeerID) {
        if type == MultiPeerDataType.string.rawValue{
            let str = String(data: data, encoding: .utf8)
            ProgressHUD.show(str, icon: .exclamation, interaction: true)
        }
        else if type == MultiPeerDataType.image.rawValue{
            
        }
        else if type == MultiPeerDataType.mappedWorld.rawValue{
            
            if let mappedWorld = decodeMappedWorld(data){
                runSesstionWithWorldMap(mappedWorld)
                
                ProgressHUD.show("Maped world data recived from \(peerID.displayName)", icon: .exclamation, interaction: true)
            }
        }
        else if type == MultiPeerDataType.anchor.rawValue{
            do{
                if let anchor = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARAnchor.self, from: data) {
                    // Add anchor to the session, ARSCNView delegate adds visible content.
                    sceneView.session.add(anchor: anchor)
                    
                    ProgressHUD.show("Anchor data recived from \(peerID.displayName)", icon: .exclamation, interaction: true)
                }
            }
            catch{
                print("error = \(error)")
            }
        }
        else if type == MultiPeerDataType.node.rawValue{
            
            if let node = decodeNode(data){
                sceneView.scene.rootNode.addChildNode(node)
                
                ProgressHUD.show("Node data recived from \(peerID.displayName)", icon: .exclamation, interaction: true)
            }

        }
    }
    func multiPeer(connectedDevicesChanged devices: [String]) {
        for str in devices{
            ProgressHUD.show("\(str) device just connected", icon: .exclamation, interaction: true)
            connectedDeviceName.text = str
            connectedDeviceName.sizeToFit()
        }
        
        if devices.isEmpty {
            ProgressHUD.show("There are no connected devices", icon: .exclamation, interaction: true)
            connectedDeviceName.text = "-"
        }
        
        connectedDevicesCountLabel.text = "Connected devices: \(devices.count)"
        connectedDevicesCountLabel.sizeToFit()
    }
    
    //MARK: - textfield delegate
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        guard textField.text != nil, textField.text!.count > 0 else{
            return
        }
        if let image = FileManagerHelper.getImageUsedForTracking(){
            let size = image.pngData()!.count
            let key = String(size)
            
            UserDefaults.standard.setValue(textField.text, forKey: key)
            
            let configuration = configureARWith(trackingMode: .imageTracking)
            initiateSceneSessionWithConfiguration(configuration)
        }
        
        textField.removeFromSuperview()
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    //MARK: - helpers
    func encodeNode(_ node: SCNNode) -> Data?{
        guard let data = try? NSKeyedArchiver.archivedData(withRootObject: node, requiringSecureCoding: true) else {
            NSLog("Error: Could not archive node \(node)")
            return nil
        }
        
        return data
    }
    func encodeMappedWorld(_ map: ARWorldMap) -> Data?{
        guard let data = try? NSKeyedArchiver.archivedData(withRootObject: map, requiringSecureCoding: true) else {
            NSLog("Error: Could not archive mapped world \(map)")
            return nil
        }
        
        return data
    }
    func decodeMappedWorld(_ data: Data) -> ARWorldMap?{
        do{
            if let map = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: data) {
                return map
            }
        }
        catch{
            NSLog("Error: could not decode mapped world from data \(data)")
            print("Error: \(error)")
        }
        
        return nil
    }
    func decodeNode(_ data: Data) -> SCNNode?{
        do{
            if let node = try NSKeyedUnarchiver.unarchivedObject(ofClass: SCNNode.self, from: data) {
                return node
            }
        }
        catch{
            NSLog("Error: could not decode node from data \(data)")
            print("Error: \(error)")
        }
        
        return nil
    }
    
}
