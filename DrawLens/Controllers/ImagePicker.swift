//
//  ImagePicker.swift
//  DrawLens
//
//  Created by Sebastian Ganea on 14/01/2021.
//

import UIKit

class ImagePicker: UIImagePickerController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imageSelecterCallback: ((UIImage)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        self.mediaTypes = ["public.image"]
        self.sourceType = .photoLibrary
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       
    }
    

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let editedImage = info[.editedImage] as? UIImage
        let originalImage = info[.originalImage] as? UIImage
        
        if editedImage != nil{
            userSelectedImage(editedImage!)
        }
        else if originalImage != nil{
            userSelectedImage(originalImage!)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func userSelectedImage(_ image: UIImage){
        if imageSelecterCallback != nil{
            imageSelecterCallback!(image)
        }
        
        
    }

}
